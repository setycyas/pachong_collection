'''处理url的类,在给定域名和目录的前提下处理url和beautiful soup'''

from urllib.parse import urlparse
import re

class UrlHandler:

  def __init__(self, topUrl):
    """指定首页,提取域名和目录"""
    parse = self.parseUrl(topUrl)
    self._domain = parse['domain'] # 域名
    self._path = parse['path'] # 目录
    return
   
  def parseUrl(self, url):
    """把一个url分解成域名,路径,文件名.
    其中路径含有http开头"""
    result = {}
    parse = urlparse(url)
    result['domain'] = parse.scheme+'://'+parse.netloc # 域名
    result['filename'] = re.search(r'([^/]*)$', url).group(0) # topUrl的文件名
    result['path'] = url[:len(url)-len(result['filename'])] # 目录
    return result
   
  def getFullUrl(self, url, parentUrl=None):
    """把一个可能是相对的url变成绝对url,需要指定父url进行参照.
    如缺省parentUrl,则按照本对象设定处理"""
    ## 如果url以https://或http://开头,则已是完整url
    if (url.startswith(r'https://') or url.startswith(r'http://')):
      return url
    ## 分析域名和目录
    if parentUrl:
      parse = self.parseUrl(parentUrl)
    else:
      parse = {'domain': self._domain, 'path': self._path}
    ## 如果以/开头,则处于域名根目录之下
    if (url.startswith(r'/')):
      return parse['domain']+url
    ## 如果以..开头,则向上一级
    if (url.startswith(r'..')):
      result = url.replace('..', '')
      result = re.search(r'([\s\S]*)/[^/]*/$', parse['path']).group(1)
      result = result+url.replace('..', '')
      return result
    ## 如果都不是,则处于父url的目录之下
    return parse['path']+url
    
  def show(self):
    """显示域名和目录"""
    print(f'domain = {self._domain}')
    print(f'path = {self._path}')
    return
    
  def url2filename(self, url, parentUrl=None):
    """把一个url转为文件名.
    如果在本对象设定的目录下则设定为文件名,
    如果不在目录下但在域名下则前面加_r_再加后续转换
    如果不在域名下则把地址全部转换.
    各种./?一律转为_,最后表示扩展名的.不换"""
    ## 先把url变成完整的
    url = self.getFullUrl(url, parentUrl)
    ## 初步分类
    if url.startswith(self._path):
      result = url.replace(self._path, '')
    else:
      if url.startswith(self._domain):
        result = url.replace(self._domain+'/', '')
      else:
        result = url.replace(r'http://', '')
        result = result.replace(r'https://', '')
    ## 后续转换
    result = result.replace('/', '_')
    result = result.replace('?', '_')
    if '.' in result:
      match = re.search(r'([\s\S]*)\.([^.]*)$', result)
      result = match.group(1).replace('.', '_')+'.'+match.group(2)
    return result
    
if __name__ == '__main__':
  url = r'https://langrisser.org/der/3.htm'
  uh = UrlHandler(url)
  print('----基本设定:----')
  uh.show()
  print('----变换完整url测试:----')
  childUrl1 = r'/a/b/1.htm'
  childUrl2 = r'mappage/index.htm'
  print(uh.getFullUrl(childUrl1, url))
  print(uh.getFullUrl(childUrl2, url))
  print('----url转文件名测试:----')
  pathUrl = 'abc.htm'
  domainUrl = 'https://langrisser.org/mappage/index.htm'
  fullUrl = '../l5/index.htm'
  print(uh.url2filename(pathUrl))
  print(uh.url2filename(domainUrl))
  print(uh.url2filename(fullUrl))
  
  